
# Enter your token here and rename this file to "registration_token.txt":
export REGISTRATION_TOKEN="enter token here"

# Also enter a useful name here. This is for your reference, and will appear in
# the Gitlab GUI
export RUNNER_NAME="Gitlab custom runner"

# If you'd like this runner to be selectable via tags, uncomment the below
# line and choose a tag
# export RUNNER_TAG_LIST="docker,largehd"