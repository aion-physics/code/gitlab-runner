Docker-managed Gitlab runner
============================

This repository hosts setup for a gitlab runner launched by docker.

To start the runner, run `docker compose up -d`. This will automatically restart
if your computer reboots. To stop it, run `docker compose down`. 

Setup
-----

You need to provide the runner with a registration token and a name to register
it to your gitlab group / project. Copy the file "registration_token_template.sh" and
rename it to "registration_token.sh", then edit it to add the name of your
runner and its token (retrieved from Gitlab). Then, if you already started it,
restart it. 

Reconfiguration
---------------

The runners that are spawned will remember their configuration between restarts,
so that they don't get duplicated in the Gitlab interface. If you need them to
reload their configuration, e.g. because you've changed it, reload their stored
configuration too via

    docker compose down
    docker compose up --build
