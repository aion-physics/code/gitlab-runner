FROM gitlab/gitlab-runner

ADD register_if_required.sh /register.sh
RUN chmod +x /register.sh

ADD config.toml /etc/gitlab-runner/config.toml

ENTRYPOINT ["/bin/bash"]
CMD ["/register.sh"]