#!/bin/bash

# Load registration token into environmental variables
source /run/secrets/registration_token

# Register runned if not already registered
FILE=/etc/gitlab-runner/gitlab-registered
if [[ -f "$FILE" ]]; then
    echo "Gitlab already registered"
else
	echo "Registering gitlab runner"
	gitlab-runner register && touch $FILE
fi

# Launch runner
gitlab-runner run
